public class MultiDimensionalArrayDemo {
    public static void main(String[] args) {
        int a[][]={
                {1,2,3},
                {12,32,45,22},
                {85,4,56,2,5}
        };
        for (int i=0; i<a.length; i++){
            for (int j=0;j<a[i].length;j++){
                System.out.print(a[i][j]+"\t");
            }
            System.out.println("");
        }
    }
}
