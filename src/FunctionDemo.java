public class FunctionDemo {
    public static void main(String[] args) {
        System.out.println("Rectangle#1 Area:" + rectangleArea(8, 6));
        System.out.println("Rectangle#1 Perimeter: " + rectanglePerimeter(8, 6));

        System.out.println("Rectangle#2 Area:" + rectangleArea(10, 5));
        System.out.println("Rectangle#2 Perimeter: " + rectanglePerimeter(10, 5));

        System.out.println("Rectangle#3 Area:" + rectangleArea(4, 3));
        System.out.println("Rectangle#3 Perimeter: " + rectanglePerimeter(4, 3));

    }

    public static int rectangleArea(int l, int d) {
        return l * d;
    }

    public static int rectanglePerimeter(int l, int d) {
        return 2 * (l + d);
    }



}
