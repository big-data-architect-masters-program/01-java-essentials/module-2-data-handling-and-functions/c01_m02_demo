public class MethodOverloadingDemo {
    public static void main(String[] args) {
        Area a = new Area();
        a.calculate(6);
        a.calculate(3,4f);
    }
}

class Area {
    void calculate(int side) {
        System.out.println("Area of Square: " + (side * side));
    }

    void calculate(int l, int d) {
        System.out.println("Area of Rectangle: " + (l * d));
    }

    void calculate(float l, int d) {
        System.out.println("Area of Rectangle: " + (l * d));
    }

    void calculate(int l, float d) {
        System.out.println("Area of Rectangle: " + (l * d));
    }
}