public class CallByRef {
    public static void main(String[] args) {
        Sample obj1 = new Sample(6);
        increase(obj1);
        obj1.show();
    }

    static void increase(Sample p){
        p.data = p.data + 1;
    }
}

class Sample {
    int data;

    public Sample(int d) {
        data = d;
    }

    void show() {
        System.out.println("Data: " + data);
    }
}
