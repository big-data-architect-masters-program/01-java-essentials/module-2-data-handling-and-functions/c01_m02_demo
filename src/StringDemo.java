public class StringDemo {
    public static void main(String[] args) {
        String s1 = "Hello World";
        String s2 = "    Tony Stark    ";
        String s3 = "hello world";
        System.out.println("Length of s1 = " + s1.length());
        System.out.println("Index 3 of s1 = " + s1.charAt(3));
        System.out.println("Concatenate of s1 & s1 = " + s1.concat(" is " + s1));
        System.out.println("Is s1 = s2? " + s1.equals(s2));
        System.out.println("Is s1 = s3? " + s1.equals(s3));
        System.out.println("Is s1 = s3? " + s1.equalsIgnoreCase(s3));
        System.out.println("Index of n in s2 = "+s2.indexOf("n"));
        System.out.println("s1, Replace o with a : "+s1.replace('o','a'));
        System.out.println("Lower case of s1: "+s1.toLowerCase());
        System.out.println("Upper case of s1: "+s1.toUpperCase());
        System.out.println("Not Trim of s2: |"+s2+"|");
        System.out.println("Trim of s2: |"+s2.trim()+"|");
    }
}
